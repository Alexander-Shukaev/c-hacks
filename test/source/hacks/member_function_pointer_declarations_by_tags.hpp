/// Preamble {{{
//  ==========================================================================
//        @file member_function_pointer_declarations_by_tags.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-08 Monday 00:17:45 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-28 Sunday 19:11:42 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef MEMBER_FUNCTION_POINTER_DECLARATIONS_BY_TAGS_HPP_INCLUDED
# define MEMBER_FUNCTION_POINTER_DECLARATIONS_BY_TAGS_HPP_INCLUDED
//
# include "test/safe.hpp"
//
# define  USING_HACKS_MEMBER_FUNCTION_POINTER
# include <hacks/member_function_pointer>
//
namespace test {
namespace type_tags {
TEST_SAFE_MEMBER_FUNCTION_POINTER_DECLARATIONS_BY_TAGS;
} // namespace type_tags
} // namespace test
//
# endif // MEMBER_FUNCTION_POINTER_DECLARATIONS_BY_TAGS_HPP_INCLUDED

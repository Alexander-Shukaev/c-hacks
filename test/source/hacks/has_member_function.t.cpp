/// Preamble {{{
//  ==========================================================================
//        @file has_member_function.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-24 Tuesday 12:05:52 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-28 Sunday 12:02:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "has_member_function_definitions.hpp"
//
# include <hacks/t>
//
# include <utility>
// clang-format on
//
using std::pair;
//
namespace {
struct one {
  int
  function1() const;
  //
  pair<bool, char>
  function2(double, short*) const;
  //
  pair<bool, char>
  function2(float, void*) const;
};
//
struct two {
  bool
  function2(double, short*) const;
  //
  void
  function3(pair<bool, char>, int, one&);
};
//
T_TEST_SUITE(has_member_function)
//
T_TEST_CASE(HAS_MEMBER_FUNCTION) {
  // clang-format off
  T_CHECK(!HAS_MEMBER_FUNCTION(int, one, function1, ()));
  T_CHECK( HAS_MEMBER_FUNCTION(int, one, function1, () const));
  //
  T_CHECK(!HAS_MEMBER_FUNCTION(int, two, function1, ()));
  T_CHECK(!HAS_MEMBER_FUNCTION(int, two, function1, () const));
  //
  T_CHECK(!HAS_MEMBER_FUNCTION((pair<bool, char>),
                               one,
                               function2,
                               (double, short*)));
  T_CHECK( HAS_MEMBER_FUNCTION((pair<bool, char>),
                               one,
                               function2,
                               (double, short*) const));
  T_CHECK(!HAS_MEMBER_FUNCTION(char,
                               one,
                               function2,
                               (double, short*)));
  T_CHECK(!HAS_MEMBER_FUNCTION(char,
                               one,
                               function2,
                               (double, short*) const));
  T_CHECK(!HAS_MEMBER_FUNCTION((pair<bool, char>),
                               one,
                               function2,
                               (float, short*)));
  T_CHECK(!HAS_MEMBER_FUNCTION((pair<bool, char>),
                               one,
                               function2,
                               (float, short*) const));
  //
  T_CHECK(!HAS_MEMBER_FUNCTION((pair<bool, char>),
                               one,
                               function2,
                               (float, void*)));
  T_CHECK( HAS_MEMBER_FUNCTION((pair<bool, char>),
                               one,
                               function2,
                               (float, void*) const));
  T_CHECK(!HAS_MEMBER_FUNCTION(char,
                               one,
                               function2,
                               (float, void*)));
  T_CHECK(!HAS_MEMBER_FUNCTION(char,
                               one,
                               function2,
                               (float, void*) const));
  T_CHECK(!HAS_MEMBER_FUNCTION((pair<bool, char>),
                               one,
                               function2,
                               (double, void*)));
  T_CHECK(!HAS_MEMBER_FUNCTION((pair<bool, char>),
                               one,
                               function2,
                               (double, void*) const));
  //
  T_CHECK(!HAS_MEMBER_FUNCTION(bool,
                               two,
                               function2,
                               (double, short*)));
  T_CHECK( HAS_MEMBER_FUNCTION(bool,
                               two,
                               function2,
                               (double, short*) const));
  T_CHECK(!HAS_MEMBER_FUNCTION(char,
                               two,
                               function2,
                               (double, short*)));
  T_CHECK(!HAS_MEMBER_FUNCTION(char,
                               two,
                               function2,
                               (double, short*) const));
  T_CHECK(!HAS_MEMBER_FUNCTION(bool,
                               two,
                               function2,
                               (float, short*)));
  T_CHECK(!HAS_MEMBER_FUNCTION(bool,
                               two,
                               function2,
                               (float, short*) const));
  //
  T_CHECK( HAS_MEMBER_FUNCTION(void,
                               two,
                               function3,
                               (pair<bool, char>, int, one&)));
  T_CHECK(!HAS_MEMBER_FUNCTION(void,
                               two,
                               function3,
                               (pair<bool, char>, int, one&) const));
  T_CHECK(!HAS_MEMBER_FUNCTION(void,
                               two,
                               function3,
                               (pair<bool, char>, int, one)));
  T_CHECK(!HAS_MEMBER_FUNCTION(void,
                               two,
                               function3,
                               (pair<bool, char>, int, one) const));
  T_CHECK(!HAS_MEMBER_FUNCTION(char,
                               two,
                               function3,
                               (pair<bool, char>, int, one&)));
  T_CHECK(!HAS_MEMBER_FUNCTION(char,
                               two,
                               function3,
                               (pair<bool, char>, int, one&) const));
  T_CHECK(!HAS_MEMBER_FUNCTION(void,
                               two,
                               function3,
                               (pair<bool, char>&, int, one&)));
  T_CHECK(!HAS_MEMBER_FUNCTION(void,
                               two,
                               function3,
                               (pair<bool, char>&, int, one&) const));
  T_CHECK(!HAS_MEMBER_FUNCTION(void,
                               two,
                               function3,
                               (pair<bool, char>, short, one&)));
  T_CHECK(!HAS_MEMBER_FUNCTION(void,
                               two,
                               function3,
                               (pair<bool, char>, short, one&) const));
  // clang-format on
}
//
T_TEST_SUITE_END
} // namespace

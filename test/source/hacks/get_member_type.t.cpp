/// Preamble {{{
//  ==========================================================================
//        @file get_member_type.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-24 Tuesday 12:05:52 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-08-07 Sunday 00:50:53 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "get_member_type_definitions.hpp"
//
# include <hacks/t>
//
# include <boost/type_traits/is_same.hpp>
// clang-format on
//
namespace {
struct point {
  typedef float x_type;
  typedef float y_type;
  //
  struct inner {};
};
//
struct POINT {
  typedef float X_TYPE;
  typedef float Y_TYPE;
  //
  struct INNER {};
};
//
T_TEST_SUITE(get_member_type)
//
T_TEST_CASE(GET_MEMBER_TYPE) {
  using boost::is_same;
  //
  // clang-format off
  T_CHECK((is_same<GET_MEMBER_TYPE   (point, x_type), point::x_type>::value));
  T_CHECK((is_same<GET_MEMBER_TYPE   (point, X_TYPE),          void>::value));
  T_CHECK((is_same<GET_MEMBER_TYPE_OR(point, X_TYPE, POINT),  POINT>::value));
  //
  T_CHECK((is_same<GET_MEMBER_TYPE   (point, inner),   point::inner>::value));
  T_CHECK((is_same<GET_MEMBER_TYPE   (point, INNER),           void>::value));
  T_CHECK((is_same<GET_MEMBER_TYPE_OR(point, INNER, bool),     bool>::value));
  //
  T_CHECK((is_same<GET_MEMBER_TYPE   (POINT, x_type),          void>::value));
  T_CHECK((is_same<GET_MEMBER_TYPE_OR(POINT, x_type, point),  point>::value));
  T_CHECK((is_same<GET_MEMBER_TYPE   (POINT, X_TYPE), POINT::X_TYPE>::value));
  //
  T_CHECK((is_same<GET_MEMBER_TYPE   (POINT, inner),           void>::value));
  T_CHECK((is_same<GET_MEMBER_TYPE_OR(POINT, inner, char),     char>::value));
  T_CHECK((is_same<GET_MEMBER_TYPE   (POINT, INNER),   POINT::INNER>::value));
  // clang-format on
}
//
T_TEST_SUITE_END
} // namespace

/// Preamble {{{
//  ==========================================================================
//        @file safe.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-08-07 Sunday 22:43:58 (+0200)
//  --------------------------------------------------------------------------
//     @created 2016-02-28 Sunday 19:03:51 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "safe.hpp"
// clang-format on
//
using std::vector;
//
namespace test {
safe::safe(bool b, char c, float f, int i)
    : _bool(b), _char(c), _float(f), _int(i) {
}
//
bool
safe::operator==(safe const& s) const {
  // clang-format off
  return _bool     == s._bool   &&
         _char     == s._char   &&
         _float    == s._float  &&
         _int      == s._int    &&
         _vector   == s._vector;
  // clang-format on
}
//
bool
safe::operator!=(safe const& s) const {
  return !(*this == s);
}
//
bool
safe::get_bool() const {
  return _bool;
}
//
char
safe::get_char() const {
  return _char;
}
//
float
safe::get_float() const {
  return _float;
}
//
int
safe::get_int() const {
  return _int;
}
//
::vector<safe*> const&
safe::get_vector() const {
  return _vector;
}
//
void
safe::set(bool b) {
  _bool = b;
}
//
void
safe::set(char c) {
  _char = c;
}
//
void
safe::set(float f) {
  _float = f;
}
//
void
safe::set(int i) {
  _int = i;
}
//
void
safe::set(bool b, char c, float f, int i) {
  _bool  = b;
  _char  = c;
  _float = f;
  _int   = i;
}
//
void
safe::set(::vector<safe*> const& v) {
  _vector = v;
}
} // namespace test

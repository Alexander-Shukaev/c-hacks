/// Preamble {{{
//  ==========================================================================
//        @file safe.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-24 Tuesday 12:49:49 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-28 Sunday 18:54:41 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef TEST_SAFE_HPP_INCLUDED
# define TEST_SAFE_HPP_INCLUDED
//
# include <hacks/t>
//
# include <vector>
// clang-format on
//
namespace test {
class safe {
public:
  safe(bool, char, float, int);
  //
public:
  bool
  operator==(safe const&) const;
  //
  bool
  operator!=(safe const&) const;
  //
private:
  bool
  get_bool() const;
  //
  char
  get_char() const;
  //
  float
  get_float() const;
  //
  int
  get_int() const;
  //
  ::std::vector<safe*> const&
  get_vector() const;
  // -------------------------------------------------------------------------
  void
  set(bool);
  //
  void
  set(char);
  //
  void
  set(float);
  //
  void
  set(int);
  //
  void
  set(bool, char, float, int);
  //
  void
  set(::std::vector<safe*> const&);
  //
private:
  bool  _bool;
  char  _char;
  float _float;
  int   _int;
  //
  ::std::vector<safe*> _vector;
}; // class safe
} // namespace test
//
namespace {}
//
T_LOG_DISABLE_BY_TYPE(::test::safe)
//
// clang-format off
# define TEST_SAFE_MEMBER_FUNCTION_POINTER_DECLARATIONS \
    MEMBER_FUNCTION_POINTER_DECLARATION( \
        bool,  ::test::safe, get_bool,  () const); \
    MEMBER_FUNCTION_POINTER_DECLARATION( \
        char,  ::test::safe, get_char,  () const); \
    MEMBER_FUNCTION_POINTER_DECLARATION( \
        float, ::test::safe, get_float, () const); \
    MEMBER_FUNCTION_POINTER_DECLARATION( \
        int,   ::test::safe, get_int,   () const); \
    \
    MEMBER_FUNCTION_POINTER_DECLARATION( \
        ::std::vector< ::test::safe*> const&, \
        ::test::safe, \
        get_vector, \
        () const); \
    \
    MEMBER_FUNCTION_POINTER_DECLARATION(void, ::test::safe, set, (bool )); \
    MEMBER_FUNCTION_POINTER_DECLARATION(void, ::test::safe, set, (char )); \
    MEMBER_FUNCTION_POINTER_DECLARATION(void, ::test::safe, set, (float)); \
    MEMBER_FUNCTION_POINTER_DECLARATION(void, ::test::safe, set, (int  )); \
    \
    MEMBER_FUNCTION_POINTER_DECLARATION(void, ::test::safe, set, (bool, \
                                                                  char, \
                                                                  float, \
                                                                  int)); \
    \
    MEMBER_FUNCTION_POINTER_DECLARATION( \
        void, \
        ::test::safe, \
        set, \
        (::std::vector< ::test::safe*> const&))
//
# define TEST_SAFE_MEMBER_FUNCTION_POINTER_DEFINITIONS \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, get_bool,  () const); \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, get_char,  () const); \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, get_float, () const); \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, get_int,   () const); \
    \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, get_vector, () const); \
    \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, set, (bool )); \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, set, (char )); \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, set, (float)); \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, set, (int  )); \
    \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, set, (bool, \
                                                           char, \
                                                           float, \
                                                           int)); \
    \
    MEMBER_FUNCTION_POINTER_DEFINITION(::test::safe, \
                                       set, \
                                       (::std::vector< ::test::safe*> const&))
//
# define TEST_SAFE_MEMBER_FUNCTION_POINTER(FUNCTION_NAME, \
                                           PARAMETER_DECLARATION) \
    MEMBER_FUNCTION_POINTER(::test::safe, \
                            FUNCTION_NAME, \
                            PARAMETER_DECLARATION)
//
# define TEST_SAFE_MEMBER_OBJECT_POINTER_DECLARATIONS \
    MEMBER_OBJECT_POINTER_DECLARATION(bool,  ::test::safe, _bool ); \
    MEMBER_OBJECT_POINTER_DECLARATION(char,  ::test::safe, _char ); \
    MEMBER_OBJECT_POINTER_DECLARATION(int,   ::test::safe, _int  ); \
    MEMBER_OBJECT_POINTER_DECLARATION(float, ::test::safe, _float); \
    \
    MEMBER_OBJECT_POINTER_DECLARATION(::std::vector< ::test::safe*>, \
                                      ::test::safe, \
                                      _vector)
//
# define TEST_SAFE_MEMBER_OBJECT_POINTER_DEFINITIONS \
    MEMBER_OBJECT_POINTER_DEFINITION(::test::safe, _bool ); \
    MEMBER_OBJECT_POINTER_DEFINITION(::test::safe, _char ); \
    MEMBER_OBJECT_POINTER_DEFINITION(::test::safe, _int  ); \
    MEMBER_OBJECT_POINTER_DEFINITION(::test::safe, _float); \
    \
    MEMBER_OBJECT_POINTER_DEFINITION(::test::safe, _vector)
//
# define TEST_SAFE_MEMBER_OBJECT_POINTER(OBJECT_NAME) \
    MEMBER_OBJECT_POINTER(::test::safe, OBJECT_NAME)
//
# define TEST_SAFE_MEMBER_FUNCTION_POINTER_DECLARATIONS_BY_TAGS \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_get_bool,  bool,  ::test::safe, () const); \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_get_char,  char,  ::test::safe, () const); \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_get_float, float, ::test::safe, () const); \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_get_int,   int,   ::test::safe, () const); \
    \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_get_vector, \
        ::std::vector< ::test::safe*> const&, \
        ::test::safe, \
        () const); \
    \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_set_bool,  void, ::test::safe, (bool )); \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_set_char,  void, ::test::safe, (char )); \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_set_float, void, ::test::safe, (float)); \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_set_int,   void, ::test::safe, (int  )); \
    \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_set_all, void, ::test::safe, (bool, char, float, int)); \
    \
    MEMBER_FUNCTION_POINTER_DECLARATION_BY_TAG( \
        safe_set_vector, \
        void, \
        ::test::safe, \
        (::std::vector< ::test::safe*> const&))
//
# define TEST_SAFE_MEMBER_FUNCTION_POINTER_DEFINITIONS_BY_TAGS \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_get_bool,  ::test::safe, get_bool ); \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_get_char,  ::test::safe, get_char ); \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_get_float, ::test::safe, get_float); \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_get_int,   ::test::safe, get_int  ); \
    \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_get_vector, ::test::safe, get_vector); \
    \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_set_bool,  ::test::safe, set); \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_set_char,  ::test::safe, set); \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_set_float, ::test::safe, set); \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_set_int,   ::test::safe, set); \
    \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_set_all, ::test::safe, set); \
    \
    MEMBER_FUNCTION_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_set_vector, ::test::safe, set)
//
# define TEST_SAFE_MEMBER_FUNCTION_POINTER_BY_TAG(TAG_NAME) \
    MEMBER_FUNCTION_POINTER_BY_TAG(TAG_NAME)
//
# define TEST_SAFE_MEMBER_OBJECT_POINTER_DECLARATIONS_BY_TAGS \
    MEMBER_OBJECT_POINTER_DECLARATION_BY_TAG( \
        safe_bool,  bool,  ::test::safe); \
    MEMBER_OBJECT_POINTER_DECLARATION_BY_TAG( \
        safe_char,  char,  ::test::safe); \
    MEMBER_OBJECT_POINTER_DECLARATION_BY_TAG( \
        safe_float, float, ::test::safe); \
    MEMBER_OBJECT_POINTER_DECLARATION_BY_TAG( \
        safe_int,   int,   ::test::safe); \
    \
    MEMBER_OBJECT_POINTER_DECLARATION_BY_TAG( \
        safe_vector, ::std::vector< ::test::safe*>, ::test::safe)
//
# define TEST_SAFE_MEMBER_OBJECT_POINTER_DEFINITIONS_BY_TAGS \
    MEMBER_OBJECT_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_bool,  ::test::safe, _bool ); \
    MEMBER_OBJECT_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_char,  ::test::safe, _char ); \
    MEMBER_OBJECT_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_float, ::test::safe, _float); \
    MEMBER_OBJECT_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_int,   ::test::safe, _int  ); \
    \
    MEMBER_OBJECT_POINTER_DEFINITION_BY_TAG( \
        ::test::type_tags::safe_vector, ::test::safe, _vector)
//
# define TEST_SAFE_MEMBER_OBJECT_POINTER_BY_TAG(TAG_NAME) \
    MEMBER_OBJECT_POINTER_BY_TAG(TAG_NAME)
//
# endif // TEST_SAFE_HPP_INCLUDED

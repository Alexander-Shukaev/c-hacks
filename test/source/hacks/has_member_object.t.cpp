/// Preamble {{{
//  ==========================================================================
//        @file has_member_object.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-24 Tuesday 12:05:52 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-28 Sunday 12:02:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "has_member_object_definitions.hpp"
//
# include <hacks/t>
// clang-format on
//
namespace {
struct point {
  float x;
  float y;
};
//
struct POINT {
  float X;
  float Y;
};
//
T_TEST_SUITE(has_member_object)
//
T_TEST_CASE(HAS_MEMBER_OBJECT) {
  // clang-format off
  T_CHECK( HAS_MEMBER_OBJECT(point, x));
  T_CHECK(!HAS_MEMBER_OBJECT(point, X));
  //
  T_CHECK(!HAS_MEMBER_OBJECT(POINT, x));
  T_CHECK( HAS_MEMBER_OBJECT(POINT, X));
  // clang-format on
}
//
T_TEST_SUITE_END
} // namespace

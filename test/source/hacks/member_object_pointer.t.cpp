/// Preamble {{{
//  ==========================================================================
//        @file member_object_pointer.t.cpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-24 Tuesday 12:08:13 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-28 Sunday 12:02:57 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# include "fixture.hpp"
# include "member_object_pointer_declarations.hpp"
//
# include <hacks/t>
//
# define TEST_SAFE_MOP TEST_SAFE_MEMBER_OBJECT_POINTER
// clang-format on
//
namespace {
T_TEST_SUITE_BY_FIXTURE(member_object_pointer, fixture)
//
T_TEST_CASE(MEMBER_OBJECT_POINTER_read) {
  using ::test::safe;
  //
  using ::std::vector;
  //
  // clang-format off
  bool  const b = _safe.*TEST_SAFE_MOP(_bool);
  char  const c = _safe.*TEST_SAFE_MOP(_char);
  float const f = _safe.*TEST_SAFE_MOP(_float);
  int   const i = _safe.*TEST_SAFE_MOP(_int);
  // clang-format on
  //
  T_CHECK_EQ(b, _bool);
  T_CHECK_EQ(c, _char);
  T_CHECK_EQ(f, _float);
  T_CHECK_EQ(i, _int);
  //
  vector<safe*> const& v = _safe.*TEST_SAFE_MOP(_vector);
  //
  T_CHECK(v.empty());
}
//
T_TEST_CASE(MEMBER_OBJECT_POINTER_write) {
  using ::test::safe;
  //
  using ::std::vector;
  //
  safe s(_safe);
  //
  T_REQUIRE(s == _safe);
  //
  // clang-format off
  bool  const b = !_bool;
  char  const c = _char  ^ 1;
  float const f = _float - 1;
  int   const i = _int   / 2;
  // clang-format on
  //
  // clang-format off
  s.*TEST_SAFE_MOP(_bool)  = b;
  s.*TEST_SAFE_MOP(_char)  = c;
  s.*TEST_SAFE_MOP(_float) = f;
  s.*TEST_SAFE_MOP(_int)   = i;
  // clang-format on
  //
  T_CHECK(s != _safe);
  T_CHECK(s == safe(b, c, f, i));
  //
  // clang-format off
  s.*TEST_SAFE_MOP(_bool)  = _bool;
  s.*TEST_SAFE_MOP(_char)  = _char;
  s.*TEST_SAFE_MOP(_float) = _float;
  s.*TEST_SAFE_MOP(_int)   = _int;
  // clang-format on
  //
  T_CHECK(s == _safe);
  //
  vector<safe*> v;
  v.push_back(&s);
  //
  s.*TEST_SAFE_MOP(_vector) = v;
  //
  T_CHECK(s != _safe);
  //
  v.clear();
  //
  s.*TEST_SAFE_MOP(_vector) = v;
  //
  T_CHECK(s == _safe);
}
//
T_TEST_SUITE_END
} // namespace

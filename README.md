```bash
make                                         clean b.help
make                                         clean            install
make                                         clean b.check    install
make CMAKE_OPTIONS='-DRTTI=Y'                clean b.check    install
make CMAKE_OPTIONS='-DRTTI=N'                clean b.check    install
make                          BUILDER='make' clean b.check    install
make CMAKE_OPTIONS='-DRTTI=Y' BUILDER='make' clean b.check    install
make CMAKE_OPTIONS='-DRTTI=N' BUILDER='make' clean b.check    install
make                                         clean b.memcheck install
make CMAKE_OPTIONS='-DRTTI=Y'                clean b.memcheck install
make CMAKE_OPTIONS='-DRTTI=N'                clean b.memcheck install
make                          BUILDER='make' clean b.memcheck install
make CMAKE_OPTIONS='-DRTTI=Y' BUILDER='make' clean b.memcheck install
make CMAKE_OPTIONS='-DRTTI=N' BUILDER='make' clean b.memcheck install
```

/// Preamble {{{
//  ==========================================================================
//        @file member.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2016-02-28 Sunday 11:43:37 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-02-20 Saturday 22:56:09 (+0100)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2016,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef @PP_PREFIX@MEMBER_HPP_INCLUDED
# define @PP_PREFIX@MEMBER_HPP_INCLUDED
//
# include "member_pointer"
//
# define @PP_PREFIX@MEMBER_BY_TAG( \
      TAG_NAME) \
    *@PP_PREFIX@MEMBER_POINTER_BY_TAG( \
        TAG_NAME)
//
# define @PP_PREFIX@MEMBER( \
      TAG_NAME) \
    *@PP_PREFIX@MEMBER_POINTER( \
        TAG_NAME)
//
# endif // @PP_PREFIX@MEMBER_HPP_INCLUDED
//
# if defined(USING@PP_SUFFIX@)        || \
     defined(USING@PP_SUFFIX@_MEMBER)
#   define MEMBER_BY_TAG \
      @PP_PREFIX@MEMBER_BY_TAG
#   define MEMBER \
      @PP_PREFIX@MEMBER
# endif
